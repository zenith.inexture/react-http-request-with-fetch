import "./App.css";
import GetMethod from "./components/GetMethod";
import PostMethod from "./components/PostMethod";
import PutMehod from "./components/PutMehod";
import DeleteMethod from "./components/DeleteMethod";

function App() {
  return (
    <div className="App">
      <GetMethod />
      <PostMethod />
      <PutMehod />
      <DeleteMethod />
    </div>
  );
}

export default App;
