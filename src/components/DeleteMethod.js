import React, { Component } from "react";

class DeleteMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
    };
  }

  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  deletefun = (e) => {
    e.preventDefault();
    console.log(this.state);
    //if we work in database then we can directly see the change
    fetch(`https://jsonplaceholder.typicode.com/users/${this.state.id}`, {
      method: "DELETE",
    }).then((res) => {
      console.log(res);
    });
  };

  render() {
    return (
      <>
        <h1>Delete Request</h1>
        <div>
          Id:
          <input
            type="number"
            name="id"
            value={this.state.id}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.deletefun}>
          Delete
        </button>
      </>
    );
  }
}

export default DeleteMethod;
