import React from "react";

class GetMethod extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json,
        });
        console.log(json);
      });
  }
  render() {
    const { items } = this.state;
    return (
      <div className="App">
        <h2> Fetch data from an api in react with fetch method</h2>
        <div className="item-div">
          {items.map((item) => (
            <div key={item.id}>
              <p>User_Name: {item.username}</p>
              <p>Full_Name: {item.name}</p>
              <p>User_Email:{item.email}</p>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default GetMethod;
