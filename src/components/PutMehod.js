import React, { Component } from "react";

class PutMehod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: "",
      username: "",
      email: "",
    };
  }

  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  putfun = (e) => {
    e.preventDefault();
    console.log(this.state);
    //if we work in database then we can directly see the change
    fetch(`https://jsonplaceholder.typicode.com/users/${this.state.id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: this.state.name,
        username: this.state.username,
        email: this.state.email,
      }),
    }).then((res) => {
      console.log(res);
    });
  };

  render() {
    return (
      <>
        <h1>Put Request</h1>
        <div>
          Id:
          <input
            type="number"
            name="id"
            value={this.state.id}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Name:
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.change}
          ></input>
        </div>
        <div>
          UserName:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Email:
          <input
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.putfun}>
          change
        </button>
      </>
    );
  }
}

export default PutMehod;
