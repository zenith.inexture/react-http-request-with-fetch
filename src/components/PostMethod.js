import React, { Component } from "react";

class PostMethod extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", username: "", email: "" };
  }

  //we can use formik or react-hook-form library also for onchange and validation in form
  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //we can post data from submit button
  submitfun = (e) => {
    e.preventDefault();
    console.log(this.state);
    //if we work in database then we can directly see the change
    fetch("https://jsonplaceholder.typicode.com/users", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: this.state.name,
        username: this.state.username,
        email: this.state.email,
      }),
    }).then((res) => {
      console.log(res);
    });
  };

  render() {
    return (
      <>
        <h1>Post Request</h1>
        <div>
          Name:
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.change}
          ></input>
        </div>
        <div>
          UserName:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Email:
          <input
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.submitfun}>
          Submit
        </button>
      </>
    );
  }
}

export default PostMethod;
